import React, { ChangeEvent, useState, useEffect, FC, HTMLProps } from "react";
import { useDispatch } from "react-redux";
import { setUserInfo } from "./loginSlice";
import styles from "./Login.module.css";
import { push } from "connected-react-router";

interface LoginProps extends HTMLProps<any> {}

export const Login: FC<LoginProps> = (props: LoginProps) => {
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [chatId, setChatId] = useState("");

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === "Enter") {
        dispatch(setUserInfo({ chatId, name }));
        dispatch(push("/home"));
      }
    };

    const name = localStorage.getItem("name");
    const chatId = localStorage.getItem("chatId");

    if (name) {
      setName(name);
    }

    if (chatId) {
      setChatId(chatId);
    }

    document.addEventListener("keydown", handleKeyDown);
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  });

  const handleNameChange = (e: ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
  };

  const handleChatIdChange = (e: ChangeEvent<HTMLInputElement>) => {
    setChatId(e.target.value);
  };

  const handleFormSubmit = (e: any) => {
    e.preventDefault();
    dispatch(setUserInfo({ chatId, name }));
    dispatch(push("/home"));
  };

  return (
    <div className={styles.mainContainer}>
      <form className={styles.loginForm}>
        <div>
          <label className={styles.formLabel}>
            Name:
            <input
              type="text"
              name="name"
              className={styles.formInput}
              value={name}
              maxLength={15}
              onChange={handleNameChange}
            />
          </label>
        </div>

        <div>
          <label className={styles.formLabel}>
            Chat Id:
            <input
              type="text"
              name="chatId"
              className={styles.formInput}
              value={chatId}
              maxLength={15}
              onChange={handleChatIdChange}
            />
          </label>
        </div>
        <button
          className={styles.button}
          type="button"
          onClick={handleFormSubmit}
        >
          Login
        </button>
      </form>
    </div>
  );
};

export default Login;
