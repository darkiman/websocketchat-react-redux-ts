import React, { ChangeEvent, Component, createRef } from "react";
import { io } from "socket.io-client";
import styles from "./Chat.module.css";
import TextareaAutosize from "react-textarea-autosize";
import { connect } from "react-redux";
import { setStatus } from "./chatSlice";
import { RootState } from "../../app/store";

interface ChatMessage {
  name: string;
  date: Date;
  uuid: string;
  message: string;
}

interface ChatProps {
  isConnected: boolean;
  chatId: string;
  name: string;
  dispatch: any;
}

class Chat extends Component<ChatProps> {
  socket: any = null;
  chatContainerRef: any;
  state: {
    currentMessage: string;
    chat: ChatMessage[];
  } = {
    currentMessage: "",
    chat: [],
  };

  constructor(props: any) {
    super(props);
    this.chatContainerRef = createRef();
  }

  handleKeyDown = (e: KeyboardEvent) => {
    if (e.key === "Enter" && !e.shiftKey) {
      e.preventDefault();
      this.sendMessageToChat();
    }
  };

  sendMessageToChat = () => {
    const { currentMessage } = this.state;
    if (this.state.currentMessage && this.props.isConnected) {
      this.socket.emit("sendMessage", currentMessage);
      this.setState(
        {
          currentMessage: "",
        },
        () => {
          setTimeout(() => {
            this.scrollToEndOfChat();
          }, 0);
        }
      );
    }
  };

  scrollToEndOfChat = () => {
    if (this.chatContainerRef && this.chatContainerRef.current) {
      // @ts-ignore:
      this.chatContainerRef.current.scrollTop =
        // @ts-ignore:
        this.chatContainerRef.current.scrollHeight;
    }
  };

  handleMessageChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    this.setState({
      currentMessage: e.target.value,
    });
  };

  componentDidMount() {
    const { dispatch } = this.props;

    document.addEventListener("keydown", this.handleKeyDown);

    this.socket = io("ws://localhost:3001/");

    this.socket.on("connect", () => {
      dispatch(
        setStatus({
          isConnected: true,
        })
      );
      this.socket.emit("joinChat", this.props.chatId, this.props.name);
    });

    this.socket.on("loadChat", (chatHistory: ChatMessage[]) => {
      this.setState(
        {
          chat: chatHistory,
        },
        () => {
          setTimeout(() => {
            this.scrollToEndOfChat();
          }, 0);
        }
      );
    });

    this.socket.on("newMessage", (newMessage: ChatMessage) => {
      const { chat } = this.state;
      this.setState({
        chat: chat && chat.length ? [...chat, newMessage] : [newMessage],
      });
    });

    this.socket.on("disconnect", () => {
      dispatch(
        setStatus({
          isConnected: false,
        })
      );
    });

    this.socket.connect();
  }

  componentWillUnmount(): void {
    if (this.socket) {
      this.socket.close();
    }
  }

  render() {
    const { currentMessage, chat } = this.state;
    const { isConnected } = this.props;
    return (
      <div className={styles.mainContainer}>
        <h3>Chat</h3>
        <div ref={this.chatContainerRef} className={styles.chatContainer}>
          {chat && chat.length
            ? chat.map((chatItem) => {
                return (
                  <div key={chatItem.uuid} className={styles.chatItemContainer}>
                    <span className={styles.userInfo}>
                      <span>{chatItem.name}</span>:
                    </span>
                    <span className={styles.chatMessage}>
                      {chatItem.message}
                    </span>
                  </div>
                );
              })
            : "No messages in this chat"}
        </div>
        <div>
          <TextareaAutosize
            className={styles.messageInput}
            value={currentMessage}
            maxLength={150}
            minRows={2}
            disabled={!isConnected}
            onChange={this.handleMessageChange}
          />
          <button
            className={styles.sendButton}
            type="button"
            disabled={!isConnected}
            onClick={this.sendMessageToChat}
          >
            Send
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: RootState, dispatch: any) => {
  return {
    isConnected: state.chat.isConnected,
    chatId: state.login.chatId,
    name: state.login.name,
    dispatch: dispatch,
  };
};

export default connect(mapStateToProps)(Chat);
