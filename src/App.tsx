import React from "react";
import "./App.css";
import { Route, Switch } from "react-router";
import Login from "./modules/login/Login";
import Home from "./modules/home/Home";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/login" component={Login} />
        <Route path="/home" component={Home} />
      </Switch>
    </div>
  );
}

export default App;
