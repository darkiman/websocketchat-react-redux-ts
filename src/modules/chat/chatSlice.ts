import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface ChatState {
  isConnected: boolean;
}

const initialState: ChatState = {
  isConnected: false,
};

export const chatSlice = createSlice({
  name: "chat",
  initialState,
  reducers: {
    setStatus: (state, action: PayloadAction<ChatState>) => {
      const { isConnected } = action.payload;
      state.isConnected = isConnected;
    },
  },
});

export const { setStatus } = chatSlice.actions;

// export const incrementAsync = (amount: number): AppThunk => (dispatch) => {
//   setTimeout(() => {
//     dispatch(incrementByAmount(amount));
//   }, 1000);
// };

export const selectIsConnected = (state: RootState) => state.chat.isConnected;

export default chatSlice.reducer;
