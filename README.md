React+Redux+TS. Websocket chat

## Available Scripts

In the project directory, you can run to start FE part:
### `yarn install`
### `yarn start`

In the project directory, you can run to start BE part:
### `cd server && yarn install`
### `yarn start`
