import {
  createStore,
  ThunkAction,
  Action,
  compose,
  applyMiddleware,
} from "@reduxjs/toolkit";
import { routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";
import createRootReducer from "../app/rootReducer";

export const history = createBrowserHistory();

function configureStore() {
  const store = createStore(
    createRootReducer(history),
    compose(applyMiddleware(routerMiddleware(history)))
  );

  return store;
}

export const store = configureStore();

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
