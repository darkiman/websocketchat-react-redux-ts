import React, { FC, HTMLProps } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectIsConnected } from "../chat/chatSlice";
import { selectChatId, selectName } from "../login/loginSlice";
import styles from "./Home.module.css";
import Chat from "../chat/Chat";
import { push } from "connected-react-router";

interface HomeProps extends HTMLProps<any> {}

export const Home: FC<HomeProps> = (props: HomeProps) => {
  const dispatch = useDispatch();
  const isConnected = useSelector(selectIsConnected);
  const chatId = useSelector(selectChatId);
  const name = useSelector(selectName);

  return (
    <div className={styles.mainContainer}>
      <div className={styles.headerContainer}>
        <div className={styles.statusContainer}>
          {isConnected ? (
            <>
              <div className={styles.greenCircle}></div>
              Connected
            </>
          ) : (
            <>
              <div className={styles.redCircle}></div>
              Disconnected
            </>
          )}
        </div>
        <div>ChatId: {chatId}</div>
        <div>
          <a
            onClick={() => {
              dispatch(push("/login"));
            }}
          >
            {" "}
            {name} | Logout
          </a>
        </div>
      </div>

      <Chat />
    </div>
  );
};
export default Home;
