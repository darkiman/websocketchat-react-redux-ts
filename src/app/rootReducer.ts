import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import loginReducer from "../modules/login/loginSlice";
import chatReducer from "../modules/chat/chatSlice";

const createRootReducer = (history: any) =>
  combineReducers({
    router: connectRouter(history),
    login: loginReducer,
    chat: chatReducer,
  });
export default createRootReducer;
