const { v4: uuidv4 } = require("uuid");
const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const socket = require("socket.io");
const io = socket(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
  },
});

const chats = {};
const chatMessages = {};

const deleteSocketFromChat = (socketId) => {
  for (let chatId of Object.keys(chats)) {
    const item = chats[chatId].find((s) => s.socketId === socketId);
    if (item > -1) {
      const index = chats[chatId].indexOf(item);
      chats[chatId].splice(index, 1);
      break;
    }
  }
};

const addMessageToChat = (chatId, message) => {
  if (chatMessages[chatId]) {
    chatMessages[chatId].push(message);
  } else {
    chatMessages[chatId] = [message];
  }
};

const sendNewMessageToAllSocketsInChat = (socketId, newMessage) => {
  for (let chatId of Object.keys(chats)) {
    const item = chats[chatId].find((s) => s.socketId === socketId);
    if (item) {
      const currentChat = chats[chatId];
      const data = {
        name: item.name,
        message: newMessage,
        date: new Date(),
        uuid: uuidv4(),
      };
      addMessageToChat(chatId, data);
      for (let s of currentChat) {
        console.log("send new message to", s);
        io.to(s.socketId).emit("newMessage", data);
      }
    }
  }
};

const addSocketToChat = (socketId, chatId, name) => {
  const data = {
    socketId,
    name,
  };
  if (chats[chatId]) {
    chats[chatId].push(data);
  } else {
    chats[chatId] = [data];
  }
};

const sendChatHistoryToSocket = (socketId, chatId) => {
  console.log("send hsitory to ", socketId);
  io.to(socketId).emit("loadChat", chatMessages[chatId]);
};

io.on("connection", (socket) => {
  console.log("new connection", socket.id);

  socket.once("disconnect", function () {
    console.log("socket disconnected", socket.id);
    deleteSocketFromChat(socket.id);
  });

  socket.on("joinChat", (chatId, name) => {
    console.log("new socket in chat", chatId, name);
    addSocketToChat(socket.id, chatId, name);
    sendChatHistoryToSocket(socket.id, chatId);
  });

  socket.on("sendMessage", (payload) => {
    console.log("new message from socket", socket.id);
    sendNewMessageToAllSocketsInChat(socket.id, payload);
  });
});

server.listen(3001, () => console.log("server is running on port 3001"));
