import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface LoginState {
  chatId: string | null;
  name: string | null;
}

const initialState: LoginState = {
  chatId: localStorage.getItem("chatId"),
  name: localStorage.getItem("name"),
};

export const loginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    setUserInfo: (state, action: PayloadAction<LoginState>) => {
      const { name, chatId } = action.payload;
      state.chatId = chatId;
      state.name = name;

      localStorage.setItem("chatId", chatId ? chatId : "");
      localStorage.setItem("name", name ? name : "");
    },
  },
});

export const { setUserInfo } = loginSlice.actions;

// export const incrementAsync = (amount: number): AppThunk => (dispatch) => {
//   setTimeout(() => {
//     dispatch(incrementByAmount(amount));
//   }, 1000);
// };

export const selectName = (state: RootState) => state.login.name;
export const selectChatId = (state: RootState) => state.login.chatId;

export default loginSlice.reducer;
